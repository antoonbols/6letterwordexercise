﻿using System.Collections.Generic;
using _6letterwordexercise.Common.Models;

namespace _6letterwordexercise.Business.Processor {
    /// <summary>
    /// Interface to process word parts into 6 letter words
    /// </summary>
    public interface ISixLetterWordProcessor {
        /// <summary>
        /// Processes word parts into 6 letter words that are present in the input
        /// </summary>
        /// <param name="input">Word parts and resulting words</param>
        /// <returns>The found combinations of words that make a 6 letter word</returns>
        List<LetterWordResult> FindWordResults(InputDataModel input);
    }
}
