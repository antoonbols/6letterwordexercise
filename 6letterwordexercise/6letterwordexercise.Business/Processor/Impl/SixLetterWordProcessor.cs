﻿using System.Collections.Generic;
using _6letterwordexercise.Common.Models;

namespace _6letterwordexercise.Business.Processor.Impl {
    public class SixLetterWordProcessor : ISixLetterWordProcessor {
        public List<LetterWordResult> FindWordResults(InputDataModel input) {
            var ret = new List<LetterWordResult>();

            var retShorthand = new List<string>();

            foreach(string r in input.Results) {
                var parts = new List<string>();

                foreach(string p in input.Parts) {
                    if (r.Contains(p)) {
                        parts.Add(p);
                    }
                }

                for(int i = 0; i < parts.Count; i++) {
                    for(int j = i + 1; j < parts.Count; j++) {
                        if (parts[i] == parts[j])
                            continue;
                        if(parts[i] + parts[j] == r) {
                            if (IsPresentInShorthand(parts[i], parts[j], r, ref retShorthand))
                              continue;
                            ret.Add(new LetterWordResult {
                                Result = r,
                                Parts = new List<string> {
                                    parts[i], parts[j]
                                }
                            });
                        }
                        if (parts[j] + parts[i] == r) {
                            if (IsPresentInShorthand(parts[j], parts[i], r, ref retShorthand))
                              continue;
                            ret.Add(new LetterWordResult {
                              Result = r,
                              Parts = new List<string> {
                                    parts[j], parts[i]
                                }
                            });
                        }
                    }
                }
            }

            return ret;
        }

        private bool IsPresentInShorthand(string part1, string part2, string result, ref List<string> list) {
          string shorthand = $"{part1}+{part2}={result}";

          if (list.Contains(shorthand)) {
            return true;
          }
          list.Add(shorthand);
          return false;
        }
    }
}
