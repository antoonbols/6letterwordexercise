﻿using System.Collections.Generic;
using _6letterwordexercise.Common.Models;

namespace _6letterwordexercise.Common.Mapping {
  public static class ResultMapper {
      public static List<string> FormatResult(List<LetterWordResult> result) {
          var ret = new List<string>();

          foreach(LetterWordResult r in result) {
              ret.Add($"{string.Join(" + ", r.Parts)} = {r.Result}");
          }

          return ret;
      }
  }
}
