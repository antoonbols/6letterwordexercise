﻿using _6letterwordexercise.Common.Models;

namespace _6letterwordexercise.Common.Mapping {
    public class InputDataMapper {
        public static InputDataModel MapStringToModel(string[] input) {
            var ret = new InputDataModel();

            foreach(string i in input) {
                if (i.Length > 6) {
                    continue;
                }
                if (i.Length == 6) {
                    ret.Results.Add(i);
                }
                else {
                    ret.Parts.Add(i);
                }
            }

            return ret;
        }
    }
}
