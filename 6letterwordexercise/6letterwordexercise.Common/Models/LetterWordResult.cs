﻿using System.Collections.Generic;

namespace _6letterwordexercise.Common.Models {
    public class LetterWordResult {
        public string Result { get; set; }
        public List<string> Parts { get; set; }

        public LetterWordResult() {
            Parts = new List<string>();
        }
    }
}
