﻿using System.Collections.Generic;

namespace _6letterwordexercise.Common.Models
{
    public class InputDataModel
    {
        public List<string> Parts { get; set; }

        public List<string> Results { get; set; }

        public InputDataModel() {
            Parts = new List<string>();
            Results = new List<string>();
        }
    }
}
