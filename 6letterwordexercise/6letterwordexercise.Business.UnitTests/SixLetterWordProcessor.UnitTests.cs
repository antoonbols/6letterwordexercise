﻿using System.Collections.Generic;
using _6letterwordexercise.Business.Processor;
using _6letterwordexercise.Business.Processor.Impl;
using _6letterwordexercise.Common.Models;
using Xunit;

namespace _6letterwordexercise.Business.UnitTests
{
    public class SixLetterWordProcessorUnitTests
    {
        private readonly ISixLetterWordProcessor m_sixLetterWordProcessor;
        
        [Fact]
        public void FindWordResult_ValidInput_ReturnsExpectedResult() {
            // Arrange
            var input = new InputDataModel {
                Parts = new List<string> { "fo", "obar", "z", "zam" },
                Results = new List<string> { "foobar", "foobbr" }
            };
            var expectedResult = new List<LetterWordResult> {
                new LetterWordResult { Parts = new List<string> { "fo", "obar" }, Result = "foobar" }
            };

            // Act
            List<LetterWordResult> actualResult = m_sixLetterWordProcessor.FindWordResults(input);

            // Assert
            Assert.Single(actualResult);
            Assert.Equal(expectedResult[0].Parts, actualResult[0].Parts);
            Assert.Equal(expectedResult[0].Result, actualResult[0].Result);
        }

        public SixLetterWordProcessorUnitTests() {
            m_sixLetterWordProcessor = new SixLetterWordProcessor();
        }
    }
}
