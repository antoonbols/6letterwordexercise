﻿using System.Collections.Generic;
using _6letterwordexercise.Common.Mapping;
using _6letterwordexercise.Common.Models;
using Xunit;

namespace _6letterwordexcercise.Common.UnitTests.Mapping
{
    public class ResultMapperUnitTests
    {
        [Fact]
        public void FormatResult_ValidInput_ReturnsExpectedResult() {
            // Arrange
            var input = new List<LetterWordResult> { new LetterWordResult { Parts = new List<string> { "fo", "obar" }, Result = "foobar" } };
            var expectedResult = new List<string> { "fo + obar = foobar" };

            // Act
            List<string> actualResult = ResultMapper.FormatResult(input);

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
