﻿using System.Collections.Generic;
using _6letterwordexercise.Common.Mapping;
using _6letterwordexercise.Common.Models;
using Xunit;

namespace _6letterwordexcercise.Common.UnitTests.Mapping {
    public class InputDataMapperUnitTests {
        [Fact]
        public void MapStringToModel_ValidInput_ReturnsExpectedResult() {
            // Arrange
            var input = new string[] { "fo", "obar", "foobar", "123456789" };
            var expectedResult = new InputDataModel { Parts = new List<string> { "fo", "obar" }, Results = new List<string> { "foobar" } };

            // Act
            InputDataModel actualResult = InputDataMapper.MapStringToModel(input);

            // Assert
            Assert.Equal(expectedResult.Parts, actualResult.Parts);
            Assert.Equal(expectedResult.Results, actualResult.Results);
          }
    }
}
