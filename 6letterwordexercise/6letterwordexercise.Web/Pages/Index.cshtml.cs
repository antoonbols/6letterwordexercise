﻿using System.Collections.Generic;
using System.Linq;
using _6letterwordexercise.Business.Processor;
using _6letterwordexercise.Common.Mapping;
using _6letterwordexercise.Common.Models;
using _6letterwordexercise.Web.Providers;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace _6letterwordexercise.Web.Pages
{
    public class IndexModel : PageModel
    {
        private ISixLetterWordProcessor m_sixLetterWordProcessor { get; set; }

        public List<LetterWordResult> Results { get; set; }

        public List<string> Inputs { get; set; }

        public void OnGet()
        {
            string[] input = InputProvider.Input;
            Inputs = input.ToList();

            InputDataModel dataModel = InputDataMapper.MapStringToModel(input);

            Results = m_sixLetterWordProcessor.FindWordResults(dataModel);
        }

        public IndexModel(ISixLetterWordProcessor sixLetterWordProcessor) {
            m_sixLetterWordProcessor = sixLetterWordProcessor;
        }
    }
}
