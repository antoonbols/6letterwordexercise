﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using _6letterwordexercise.Business.Processor;
using _6letterwordexercise.Business.Processor.Impl;
using _6letterwordexercise.Common.Mapping;
using _6letterwordexercise.Common.Models;

namespace _6letterwordexercise.WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\input.txt");
            string[] input = File.ReadAllLines(path);

            InputDataModel dataModel = InputDataMapper.MapStringToModel(input);

            ISixLetterWordProcessor processor = new SixLetterWordProcessor();

            InitializeComponent();

            List<LetterWordResult> result = processor.FindWordResults(dataModel);

            List<string> formattedResult = ResultMapper.FormatResult(result);

            listBoxInput.Items.AddRange(input);
            listBoxResult.Items.AddRange(formattedResult.ToArray());
        }
    }
}
